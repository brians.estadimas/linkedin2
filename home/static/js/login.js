// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
    IN.Event.on(IN, "auth", getCompanyID);
}

// Use the API call wrapper to request the member's profile data
function getProfileData() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);
}

// Handle the successful return from the API call
function displayProfileData(data){
    var user = data.values[0];
    console.log('Logged in as '+user.firstName+' '+user.lastName);
    var nama = user.firstName+' '+user.lastName
    console.log('Logged in as '+nama);
    console.log(user.pictureUrl);
    $.ajax({
        method: "POST",
        url: window.location.pathname+'add-session',
        data: {
            'id'    : user.id,
            'nama'  : nama,
            'url'   : user.pictureUrl
        },
        dataType: 'json',
        success: function (data) {
            //console.log(data)
            console.log("yes")
        }
    });
    document.getElementById('navbaar').style.display = 'block';
    document.getElementById('name').innerHTML="Welcome, "+nama
    // document.getElementById("picture").innerHTML = '<img src="'+user.pictureUrl+'" />';
    // document.getElementById("name").innerHTML = user.firstName+' '+user.lastName;
    // document.getElementById("intro").innerHTML = user.headline;
    // document.getElementById("email").innerHTML = user.emailAddress;
    // document.getElementById("location").innerHTML = user.location.name;
    // document.getElementById("link").innerHTML = '<a href="'+user.publicProfileUrl+'" target="_blank">Visit my page</a>';
    // document.getElementById('profileData').style.display = 'block';

}

// Use the API call wrapper to request the company's profile data
function getCompanyID() {     
    IN.API.Raw("/companies?format=json&is-company-admin=true")
      .method("GET")
      .result(getCompanyData)
      .error(onError);
}

function getCompanyData(data) {
    var cpnyID = data.values[0].id;
    console.log(cpnyID);


    IN.API.Raw("/companies/" + cpnyID + ":(id,name,ticker,description,specialties,company-type,website-url,locations,founded-year,logo-url)?format=json")
      .method("GET")
      .result(displayCompanyData)
      .error(onError);
}

function displayCompanyData(data){
    var user = data;
    console.log(user.name);
    console.log(user.description);
    $.ajax({
        method: "POST",
        url: window.location.pathname+'add-session',
        data: {
            json_data: JSON.stringify({
            'id'    : user.id,
            'name'  : user.name,
            'image' : user.logoUrl,
            'description' : user.description,
            'location' : user.locations,
            'company-type' : user.companyType,
            'specialties' : user.specialties,
            'website-url' : user.websiteUrl,
            'founded-year' : user.foundedYear
            })
        },
        dataType: 'json',
        success: function (data) {
            //console.log(data)
            console.log("yes")
        }
    });

}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Destroy the session of linkedin
function logout(){
    IN.User.logout(removeProfileData);
        document.getElementById('navbaar').style.display = 'none';
     $.ajax({
            method: "POST",
            url: window.location.pathname+'del-session',
            data: {
            },
            dataType: 'json',
            success: function (data) {
                console.log(data)
            }
        });
}

// Remove profile data from page
function removeProfileData(){
    document.getElementById('profileData').remove();
}