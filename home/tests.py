from django.test import TestCase
from django.test import Client
from django.urls import resolve

class UnitTest(TestCase):

	def login_url_is_exist(self):
		response = Client().get('/home/')
		self.assertEqual(response.status_code, 200)

	def test_ada_session(self):
			response_post = Client().post(
				'/home/add-session', 
				 {
	            'id'    : "12345",
	            'nama'  : "nama",
	            'url'   : "user.pictureUrl"
	        }
			)
			self.assertEqual(response_post.status_code, 200)

	def test_logout(self):
			response_post = Client().post(
				'/home/del-session', 
				 {

	        }
			)
			self.assertEqual(response_post.status_code, 302)