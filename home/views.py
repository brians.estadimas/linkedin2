from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import Message_Form
from .models import Pengguna, Message
import json

response = {}

def index(request):
	if 'logged' in request.session :
		return HttpResponseRedirect('/home/profile/')
	else :
		return render(request, 'home/login.html', response)

def profile(request):
	if 'logged' not in request.session :
		return HttpResponseRedirect('/home/')
	## end of sol
	html = 'home/profile.html'
	return render(request, html, response)
	
def dashboard(request):
	if 'logged' not in request.session:
		return HttpResponseRedirect('/home/')
	## end of sol
	else:
		kode_identitas = get_data_user(request, 'kode_identitas')
		try:
			pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
		except Exception as e:
			pengguna = create_new_user(request)

		message = Message.objects.filter(pengguna=pengguna)
        
		response['message'] = message
		response['message_form'] = Message_Form
		response["message_list"] = message
		html = 'fitur_forum/fitur_forum.html'
        
		message_list = message
		paginator = Paginator(message_list, 5)
		page = request.GET.get('page', 1)
		try:
			users = paginator.page(page)
		except PageNotAnInteger:
			users = paginator.page(1)
		except EmptyPage:
			users = paginator.page(paginator.num_pages)
       
		response["message_list"] = users
		html = 'home/fitur_forum.html'
		return render(request, html, response)

def add_message(request, id):
    response['id'] = id
    form = Message_Form(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        kode_identitas = get_data_user(request, 'kode_identitas')
        try:
            pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
        except Exception as e:
            pengguna = create_new_user(request)  
     
        response['title'] = request.POST['title'] 
        response['message'] = request.POST['message']
        message = Message(pengguna=pengguna,title=response['title'], message=response['message'], kode_message = id)
        message.save()
        response['id'] = id
        html ='home/fitur_forum.html'    
        return HttpResponseRedirect('/home/')

def paginate_page(page, data_list):
    paginator = Paginator(data_list, 5)

    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    # Get the index of the current page
    index = data.number - 1
    # This value is maximum index of your pages, so the last page - 1
    max_index = len(paginator.page_range)
    # You want a range of 10, so lets calculate where to slice the list
    start_index = index if index >= 5 else 0
    end_index = 5 if index < max_index - 5 else max_index
    # Get our new page range. In the latest versions of Django page_range returns 
    # an iterator. Thus pass it to list, to make our slice possible again.
    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data':data, 'page_range':page_range}
    return paginate_data
		
@csrf_exempt
def add_session(request):
	if request.method == 'POST':
		request.session['logged']= True
		try:
			pengguna = Pengguna.objects.get(kode_identitas = request.POST['id'])
			print("username exists")
		except Exception as e:
			pengguna = Pengguna()
			pengguna.kode_identitas = request.POST['id']
			pengguna.nama = request.POST['nama']
			pengguna.url =  request.POST['url']
			pengguna.save()
			print("user created")
		return render(request, 'home/profile.html', response)

@csrf_exempt
def del_session(request):
	if request.method == 'POST':
		request.session.flush()
		print("LOGGED OUT")	 
		return HttpResponseRedirect('/home/')