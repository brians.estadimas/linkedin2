from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from .models import Pengguna

@csrf_exempt
def add_session(request):
	if request.method == 'POST':
		request.session['logged']= True
		request.session ['id']	 = request.POST['id']
		print("LOGGED IN "+str(request.session['logged'])+" "+request.session['id'])
		try:
			pengguna = Pengguna.objects.get(kode_identitas = request.POST['id'])
			print("username exists")
		except Exception as e:
			pengguna = Pengguna()
			pengguna.kode_identitas = request.POST['id']
			pengguna.nama = request.POST['nama']
			pengguna.url =  request.POST['url']
			pengguna.save()
			print("user created")

		return HttpResponse(data)

@csrf_exempt
def del_session(request):
	if request.method == 'POST':
		del request.session['logged']
		del request.session ['id']
		print("LOGGED OUT")	 
		return HttpResponse(None)