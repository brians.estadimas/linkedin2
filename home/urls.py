from django.conf.urls import url
from .views import index, profile
from .views import add_session, del_session, add_message, dashboard

urlpatterns = [
    url(r'^$', index, name='index'),
	url(r'^profile/$', profile, name='profile'),
    url(r'add-session', add_session, name='add-session'),
    url(r'del-session', del_session, name='del-session'),
	url(r'^add_message/(?P<id>.*)/$', add_message, name='add_message'),
	url(r'^dashboard/', dashboard, name='dashboard'),
    ]