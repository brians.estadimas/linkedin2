# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-12-12 20:33
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('fitur_forum', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=160, verbose_name='Nama')),
                ('comment', models.TextField(max_length=160)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
                ('status', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='fitur_forum.Message')),
            ],
        ),
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.TextField(max_length=160)),
                ('created_date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
    ]
