from django.http import HttpResponseRedirect
from .forms import Status_Form, Comment_Form
from .models import Status, Comment
from django.shortcuts import render, redirect, get_object_or_404
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from fitur_forum.models import Message
from fitur_forum.forms import Message_Form

# Create your views here.

status_dict ={}

response = {'author': "Ling In"} #TODO Implement yourname
def index(request):
    #TODO Implement, isilah dengan 6 kata yang mendeskripsikan anda

    status = Message.objects.all().order_by('-created_date')

    response['status'] = status
    response['status_form'] = Message_Form
    response['comment_form'] = Comment_Form
    response['message_list'] = status
	
    message_list = status
    paginator = Paginator(message_list, 5)
    page = request.GET.get('page', 1)
	
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        users = paginator.page(1)
    except EmptyPage:
        users = paginator.page(paginator.num_pages)
	
    response["message_list"] = users
    html = 'tanggapan_lowongan/tanggapan_lowongan.html'
    return render(request, html, response)

def add_status(request):
	form = Message_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		status = Message(status=response['status'])
		status.save()
		return redirect('/tanggapan-lowongan/')
	else:
		return HttpResponseRedirect('/tanggapan-lowongan/')

def add_comment(request, pk):
	status = Message.objects.get(pk=pk)
	form = Comment_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['comment'] = request.POST['comment']

		comment=Comment(comment=response['comment'])
		comment.status = status

		comment.nama = request.POST['nama']

		comment.save()
		return redirect('/tanggapan-lowongan/')
	else:
		return HttpResponseRedirect('/tanggapan-lowongan/')

def delete_status(request, object_id):
	status = Message.objects.get(pk=object_id)
	status.delete()
	return HttpResponseRedirect('/tanggapan-lowongan/')

def paginate_page(page, data_list):
    paginator = Paginator(data_list, 5)

    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    # Get the index of the current page
    index = data.number - 1
    # This value is maximum index of your pages, so the last page - 1
    max_index = len(paginator.page_range)
    # You want a range of 10, so lets calculate where to slice the list
    start_index = index if index >= 5 else 0
    end_index = 5 if index < max_index - 5 else max_index
    # Get our new page range. In the latest versions of Django page_range returns 
    # an iterator. Thus pass it to list, to make our slice possible again.
    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data':data, 'page_range':page_range}
    return paginate_data
