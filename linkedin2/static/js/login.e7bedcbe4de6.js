// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
    IN.Event.on(IN, "auth", getCompanyData);
}

// Use the API call wrapper to request the member's profile data
function getProfileData() {
    IN.API.Profile("me").fields("id", "first-name", "last-name", "headline", "location", "picture-url", "public-profile-url", "email-address").result(displayProfileData).error(onError);
}

// Handle the successful return from the API call
function displayProfileData(data){
    var user = data.values[0];
    console.log('Logged in as '+user.firstName+' '+user.lastName);
    $.ajax({
        method: "POST",
        url: window.location.pathname+'add-session',
        data: {
            'id'    : user.id
        },
        dataType: 'json',
        success: function (data) {
            console.log(data)
            console.log("yes")
        }
    });

    // document.getElementById("picture").innerHTML = '<img src="'+user.pictureUrl+'" />';
    // document.getElementById("name").innerHTML = user.firstName+' '+user.lastName;
    // document.getElementById("intro").innerHTML = user.headline;
    // document.getElementById("email").innerHTML = user.emailAddress;
    // document.getElementById("location").innerHTML = user.location.name;
    // document.getElementById("link").innerHTML = '<a href="'+user.publicProfileUrl+'" target="_blank">Visit my page</a>';
    // document.getElementById('profileData').style.display = 'block';

}

// Use the API call wrapper to request the company's profile data
function getCompanyData() {     
    var cpnyID = 13603386;

    IN.API.Raw("/companies/" + cpnyID + ":(id,name,ticker,description)?format=json")
      .method("GET")
      .result(displayCompanyData)
      .error(onError);
}

function displayCompanyData(data){
    var user = data;
    console.log(user.name);
    console.log(user.description);
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Destroy the session of linkedin
function logout(){
    IN.User.logout(removeProfileData);
     $.ajax({
            method: "POST",
            url: window.location.pathname+'del-session',
            data: {
            },
            dataType: 'json',
            success: function (data) {
                console.log(data)
            }
        });
}

// Remove profile data from page
function removeProfileData(){
    document.getElementById('profileData').remove();
}