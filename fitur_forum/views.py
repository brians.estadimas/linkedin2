from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import Message_Form
from .models import Message
from .utils import *

# Create your views here.
response = {}
def index(request):
    # print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('forum:dashboard'))
    else:
        response['author'] = get_data_user(request, 'user_login')
        html = 'fitur_forum/login.html'
        return render(request, html, response)

def dashboard(request):
    
    if not 'user_login' in request.session.keys():
        return HttpResponseRedirect(reverse('forum:index'))
    else:
        kode_identitas = get_data_user(request, 'kode_identitas')
        try:
            pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
        except Exception as e:
            pengguna = create_new_user(request)

        message = Message.objects.filter(pengguna=pengguna)
        
        response['message'] = message
        response['message_form'] = Message_Form
        response["message_list"] = message
        html = 'fitur_forum/fitur_forum.html'
        
        message_list = message
        paginator = Paginator(message_list, 5)
        page = request.GET.get('page', 1)
        try:
            users = paginator.page(page)
        except PageNotAnInteger:
            users = paginator.page(1)
        except EmptyPage:
            users = paginator.page(paginator.num_pages)
       
        response["message_list"] = users
        html = 'fitur_forum/fitur_forum.html'
        return render(request, html, response)

def add_message(request, id):
    response['id'] = id
    form = Message_Form(request.POST or None)

    if (request.method == 'POST' and form.is_valid()):
        kode_identitas = get_data_user(request, 'kode_identitas')
        try:
            pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
        except Exception as e:
            pengguna = create_new_user(request)  
     
        response['title'] = request.POST['title'] 
        response['message'] = request.POST['message']
        message = Message(pengguna=pengguna,title=response['title'], message=response['message'], kode_message = id)
        message.save()
        response['id'] = id
        html ='fitur_forum/fitur_forum.html'    
        return HttpResponseRedirect('/forum/')

def paginate_page(page, data_list):
    paginator = Paginator(data_list, 5)

    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)

    # Get the index of the current page
    index = data.number - 1
    # This value is maximum index of your pages, so the last page - 1
    max_index = len(paginator.page_range)
    # You want a range of 10, so lets calculate where to slice the list
    start_index = index if index >= 5 else 0
    end_index = 5 if index < max_index - 5 else max_index
    # Get our new page range. In the latest versions of Django page_range returns 
    # an iterator. Thus pass it to list, to make our slice possible again.
    page_range = list(paginator.page_range)[start_index:end_index]
    paginate_data = {'data':data, 'page_range':page_range}
    return paginate_data